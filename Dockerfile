FROM ghcr.io/sushrut1101/docker:arch

RUN /usr/bin/ssh-keygen -A

RUN pacman -Sy --noconfirm mosh

RUN mkdir /var/run/sshd

RUN echo 'root:root' | chpasswd

RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV XDG_CACHE_HOME=/root/aosp/.cache

RUN mkdir -p /root/aosp

RUN echo 'echo "ccache, packages, files, configurations and other things outside of /root/aosp* will be deleted while changing containers. please save everything project related on /root/aosp*"'  > /etc/profile.d/welcome.sh

RUN git config --global color.ui auto

# Mosh fix
RUN sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
RUN echo 'LANG=en_US.UTF-8' > /etc/locale.conf
RUN locale-gen

ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  
# End (Mosh fix)

CMD ["/usr/sbin/sshd", "-D"]
